


<?php
	session_start();
	
	$SelectQuestions = "SELECT * FROM questions  ORDER BY question_id";
	
	$SelectComments = "SELECT * FROM comments WHERE question_id=?  ORDER BY comment_id";
	
	$insertQuestion = "INSERT INTO questions(Topic) VALUES(:value1)";
	
	$insertComment = "INSERT INTO comments(user_id,question_id,Comment) VALUES(:value1,:value2,:value3)";
	
	$findQuestion = "SELECT * FROM questions WHERE Topic=?  ORDER BY question_id";
	
	$findQuestionID = "SELECT * FROM questions WHERE question_id=?  ORDER BY question_id";
	
	$deleteComment = "DELETE FROM comments WHERE comment_id=?";
	
	$deleteThread = "DELETE FROM questions WHERE question_id=?";
	
	$findUser = "SELECT * FROM forumuser WHERE user_id=?";
	
	$username = 'root';
	$password = '';
	$hostname = "localhost";
	$database = "forum";
	try{
		$dbh = new PDO("mysql:host=$hostname;dbname=$database", $username, $password);
		$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	}catch(PDOException $e){
		echo $e->getMessage();
	}
	
	$SelectQuestionsP = $dbh->prepare($SelectQuestions);
	$SelectCommentsP = $dbh->prepare($SelectComments);
	$insertQuestionP = $dbh->prepare($insertQuestion);
	$insertCommentP = $dbh->prepare($insertComment);
	$findQuestionP = $dbh->prepare($findQuestion);
	$findQuestionIDP = $dbh->prepare($findQuestionID);
	$deleteCommentP = $dbh->prepare($deleteComment);
	$deleteThreadP = $dbh->prepare($deleteThread);
	$findUserP = $dbh->prepare($findUser);
	
	/*if($_SERVER['REQUEST_METHOD'] === 'POST'){
		$findQuestionP->execute(array($_POST['Name']));
		if(	$findQuestionP->rowCount()	!== 0){
			if(isset($_SESSION['User'])){
				$insertCommentP->execute(array(	':value1'=>$_SESSION['User'],
												':value2'=>$findQuestionP->fetch()['question_id'],
												':value3'=>$_POST['Comment']));
			}else{
				$insertCommentP->execute(array(	':value1'=>0,
												':value2'=>$_POST['Name'],
												':value3'=>$_POST['Comment']));
			}
		}else{
			$insertQuestionP->execute(array(	':value1'=>$_POST['Name']));
			
			$insertCommentP->execute(array(		':value1'=>$_SESSION['User'],
												':value2'=>$findQuestionP->fetch()['question_id'],
												':value3'=>$_POST['Comment']));
		}
	}*/
	
	if($_SERVER['REQUEST_METHOD'] === 'POST'){//if creating new comment or topic
		
		if(isset($_GET['Topic'])/*$findQuestionP->rowCount() !==0*/){//if topic is set
			if(isset($_POST['Comment']))
				if(isset($_SESSION['User'])){
					$insertCommentP->execute(array(':value1'=>$_SESSION['User'],':value2'=>$_GET['Topic'],':value3'=>$_POST['Comment']));
				}else{
					$insertCommentP->execute(array(':value1'=>0,':value2'=>$_GET['Topic'],':value3'=>$_POST['Comment']));
				}
			
		}else{	//create new if not 
			
			$findQuestionP->execute(array($_POST['Name']));
			if($findQuestionP->rowCount() === 0 ){	//check if exists
				$insertQuestionP->execute(array(':value1'=>$_POST['Name']));
				$lastQuestion=$dbh->lastInsertId();
				if(isset($_POST['Comment']))
					if(isset($_SESSION['User'])){
						$insertCommentP->execute(array(':value1'=>$_SESSION['User'],':value2'=>$lastQuestion,':value3'=>$_POST['Comment']));
					}else{
						//var_dump($lastQuestion);
						$insertCommentP->execute(array(':value1'=>1,':value2'=>$lastQuestion,':value3'=>$_POST['Comment']));
					}
			}else{
				?>
				Question name taken.
				<?php 
			}
		}
	}
	if(isset($_SESSION['User'])){
		$findUserP->execute(array($_SESSION['User']));
		$user = $findUserP->fetch();
		//var_dump($user);
		if($user['Admin']==1){
			if(isset($_GET['DelComment'])){
				$deleteCommentP->execute(array($_GET['DelComment']));
				header("location: forum.php?Topic=".$_GET['Topic']);
			}
			
			if(isset($_GET['DelThread'])){
				$deleteThreadP->execute(array($_GET['DelThread']));
				header("location: forum.php");
			}
		}else{
			?>Not Admin<?php
			
		}
	}else{
		?>Not logged in.<?php 
	}
	
	if(isset($_GET['Topic'])){
		$SelectCommentsP->execute(array($_GET['Topic']));
		$findQuestionIDP->execute(array($_GET['Topic']));
		if($SelectCommentsP->rowCount()!==0){
		?>
						
			<table border='1'>
			<?php
			foreach ($SelectCommentsP->fetchAll() as $kon4e){
			?>
					<tr>
							<td style="width:300px">
								<?=$kon4e['Comment']?>
							</td>
							<td>
								<form method="GET" action="forum.php">
									<input type="hidden" name="Topic" value="<?=$_GET['Topic']?>">
									<input type="hidden" name="DelComment" value="<?=$kon4e['comment_id']?>">
									<input type="Submit" value="Delete">
								</form>
							</td>
					</tr>
				<?php 
	 		}
	 		?>
	 		</table>
	 		<?php
		}else{
			?>
	 		No Comments
	 		<?php 
		} 
	}else{
		?>
		<Table border='1'>
		<?php 
		$SelectQuestionsP->execute();
		foreach ($SelectQuestionsP->fetchAll() as $kon4e){
		?>
				<tr>
						<td>
							<a href="forum.php?Topic=<?=$kon4e['question_id']?>"> <?=$kon4e['Topic']?> </a>
						</td>
						<td>
							<form method="GET" action="forum.php">
								<input type="hidden" name="DelThread" value="<?=$kon4e['question_id']?>">
								<input type="Submit" value="Delete">
							</form>
						</td>
				</tr>
			<?php 
 		}
		?>
		</Table>
		<?php 
	}


	if(isset($_GET['Topic'])){
	?>		
		<form method="post" action="forum.php?Topic=<?=$_GET['Topic']?>">
			<textarea name="Comment" cols="25" rows="5">
				Enter your question here...
			</textarea><br>
			<input type="submit" value="Submit" />
		</form>
	<?php 
	}else{
	?>
		<form method="post" action="forum.php">
			<input type='text' name="Name" >
			<br>
			<textarea name="Comment" cols="25" rows="5">
				Enter your question here...
			</textarea>
			<br>
			<input type="submit" value="Submit" />
		</form>
	<?php 
	}
?>